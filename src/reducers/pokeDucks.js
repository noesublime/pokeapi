/* eslint-disable default-case */
import axios from 'axios';


//constantes 
const initialData = {
    array: [],
    offset: 0,
    limit: 20
}


//types
const FETCH_POKEMONS_SUCCEEDED = 'FETCH_POKEMONS_SUCCEEDED';
const NEXT_POKEMON_SUCCEEDED = 'NEXT_POKEMON_SUCCEEDED';

 
//reducer
export default function pokeReducer(state = initialData, action){
    switch(action.type){
        case FETCH_POKEMONS_SUCCEEDED:
            return{...state, array: action.payload}
        case NEXT_POKEMON_SUCCEEDED:
            return{...state, array: action.payload.array, offset: action.payload.offset}
        default:
            return state
    };
}

//actions
export const fetchPokemonsSucceeded = () => async (dispatch, getState) => {

    //console.log('getState', getState().pokemones.offset);
    const offset =  getState().pokemones.offset;
    const limit = getState().pokemones.limit;
    
    try {
        const res = await axios.get(`https://pokeapi.co/api/v2/pokemon?offset=${offset}&limit=${limit}`)
        dispatch ({
            type: FETCH_POKEMONS_SUCCEEDED,
            payload: res.data.results
        })
    
    } catch (error) {
        console.log(error)
    }
}


export const nextPokemonAction = (numero) => async (
    dispatch,
    getState
    ) => {
        const offset = getState().pokemones.offset;
        const next = offset + numero
        try {
            const res = await axios.get(`https://pokeapi.co/api/v2/pokemon?offset=${next}&limit=20`)
            dispatch({
                type: NEXT_POKEMON_SUCCEEDED,
                payload: {
                    array: res.data.results,
                    offset: next
                }
            })
        } catch (error) {
            console.log(error)
        }

}