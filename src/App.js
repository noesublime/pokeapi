import React from 'react';
import Pokemons from './components/Pokemons';

import {Provider} from 'react-redux';
import generateStore from './reducers/store';

function App() {

  const store = generateStore()


  return (
    <Provider store={store}>
        <Pokemons/>    
    </Provider>
  );
}

export default App;
