import React from 'react';

import {
    useDispatch,
    useSelector
} from 'react-redux';

import{ Link } from 'react-router-dom';


import {fetchPokemonsSucceeded, nextPokemonAction} from '../reducers/pokeDucks';


function Pokemons() {
    const dispatch = useDispatch();

    const pokemones = useSelector(store => store.pokemones.array)


    return (
        <div>
            Lista de pokemones
            <button onClick={
            () => dispatch(fetchPokemonsSucceeded())}>
                Get pokemones
            </button>
            <button onClick={() => dispatch(nextPokemonAction(20))}>Next</button>
            <ul>
                {
                    pokemones.map(item => (
                        <li key={item.name}>
                            Nombre del pokemon <br/>
                            {item.name}
                        </li>
                    ))
                }
            </ul>
        </div>
    )
}

export default Pokemons


